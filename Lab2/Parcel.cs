﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    [Serializable]
    public class Parcel
    {
        public Parcel() { } //niezbedne do serializacji pustych

        public Parcel(int number, string status, string time)
        {
            Number = number;
            Status = status;
            Time = time;
        }

        public int Number { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }

    }
}
