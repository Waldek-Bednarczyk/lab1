﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class frmChangeParcelStatus : Form
    {
        public frmChangeParcelStatus()
        {
            InitializeComponent();
            comboBoxChangeStatus.SelectedIndex = 0;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            frmParcelMenu.status = comboBoxChangeStatus.Text;
            this.Close();
        }
    }
}
