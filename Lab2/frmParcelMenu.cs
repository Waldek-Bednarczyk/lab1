﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Lab2
{
    public partial class frmParcelMenu : Form
    {
        //lista paczek
        public List<Parcel> parcels = new List<Parcel>();
        //zmienna niezbedna do zmiany statusu zamowienia
        public static string status;
        //sciezka do xml z paczkami
        public string pathway = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\parcels.xml";
        //serializer
        XmlSerializer serializer = new XmlSerializer(typeof(List<Parcel>));

        public frmParcelMenu()
        {
            InitializeComponent();

            if (File.Exists(pathway))
            {
                TextReader streamIN = new StreamReader(pathway);
                parcels = (List<Parcel>)serializer.Deserialize(streamIN);
                streamIN.Close();
                this.dataGridViewParcel.DataSource = parcels;
            }
            bindAndSave();
        }
        //jak w nazwie
        private void bindAndSave()
        {
            BindingList<Parcel> tmp = new BindingList<Parcel>(parcels);
            dataGridViewParcel.DataSource = tmp;
            TextWriter streamOUT = new StreamWriter(pathway);
            serializer.Serialize(streamOUT, parcels);
            streamOUT.Close();
        }
        //zmiana statusu paczki
        private void buttonChangeStatus_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridViewParcel.SelectedRows)
            {
                frmChangeParcelStatus frm6 = new frmChangeParcelStatus();
                frm6.ShowDialog();
                dataGridViewParcel.Rows[item.Index].Cells[1].Value = status;
            }
            bindAndSave();
        }
        //tworzenie nowej paczki wraz z automatyczna numeracja
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;
            if (parcels.Count==0)
            {
                Parcel parcel = new Parcel(1, "W systemie", time.ToString());
                parcels.Add(parcel);
            }
            else
            {
                Parcel parcel = new Parcel(parcels[parcels.Count-1].Number+1, "W systemie", time.ToString());
                parcels.Add(parcel);
            }
            bindAndSave();
        }
        //usuwanie paczki
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridViewParcel.SelectedRows)
            {
                dataGridViewParcel.Rows.RemoveAt(item.Index);
            }
            bindAndSave();
        }
    }
}