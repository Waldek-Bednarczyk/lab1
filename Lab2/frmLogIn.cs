﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Lab2
{
    public partial class frmLogIn : Form
    {
        //pierwsze użycie - tylko dla admina
        private string _adminPassword="admin";
        private string _adminLogin = "admin";
        //zmienna do przechowywania loginu
        private string _login;
        public string pathway = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\users.xml";
        public List<User> users = new List<User>();
        XmlSerializer serializer = new XmlSerializer(typeof(List<User>));

        public frmLogIn()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            bool checkIfLoggedIn = false;
            if (File.Exists(pathway))
            {
                TextReader streamIN = new StreamReader(pathway);
                users = (List<User>)serializer.Deserialize(streamIN);
                streamIN.Close();
            }
            foreach (var user in users)
            {
                _login = user.Name + user.Surname;
                if (textBoxPassword.Text == user.Password && textBoxUser.Text == _login)
                {
                    textBoxPassword.Text = "";
                    textBoxUser.Text = "";
                    frmMainMenu frm4 = new frmMainMenu();
                    frm4.ShowDialog();
                    checkIfLoggedIn = true;
                    break;
                }
            }
            if (textBoxPassword.Text == _adminPassword && textBoxUser.Text == _adminLogin)
            {
                textBoxPassword.Text = "";
                textBoxUser.Text = "";
                frmMainMenu frm4 = new frmMainMenu();
                frm4.ShowDialog();
                checkIfLoggedIn = true;
            }
            if(!checkIfLoggedIn) MessageBox.Show("Podano złą parę użytkownik/hasło", "Uwaga", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonCheckStatus_Click(object sender, EventArgs e)
        {
            frmCheckStatus frmtmp = new frmCheckStatus();
            frmtmp.ShowDialog();
        }
    }
}
