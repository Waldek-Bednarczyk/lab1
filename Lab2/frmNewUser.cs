﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
    public partial class frmNewUser : Form
    {
        private List<User> _users;
        private int _index = -1;
        private string _password;

        public frmNewUser(ref List<User> users)
        {
            _users = users;
            InitializeComponent();
        }

        public frmNewUser(ref List<User> users, ref string cellName, ref string cellSurname, ref string cellAdress, ref string password, int itemIndex)
        {
            InitializeComponent();
            _users = users;
            _password = password;
            _index = itemIndex;
            textBoxName.Text = cellName;
            textBoxSurname.Text = cellSurname;
            textBoxAdress.Text = cellAdress;
        }

        private void buttonAddEdit_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text != "" && textBoxSurname.Text != "" && textBoxAdress.Text != "")
            {
                //edycja uzytkownika - haslo pozostaje takie samo
                if (_index != -1)
                {
                    User user = new User(textBoxName.Text, textBoxSurname.Text, textBoxAdress.Text, _password);
                    _users.RemoveAt(_index);
                    _users.Insert(_index, user);
                    _index = -1;
                    this.Close();
                }
                //tworzenie nowego uzytkownika wraz z generowaniem hasla
                else
                {
                    Random rnd = new Random();
                    string newPassword = textBoxName.Text[0].ToString() + textBoxSurname.Text[0].ToString() + rnd.Next(10000, 100000).ToString();
                    User user = new User(textBoxName.Text, textBoxSurname.Text, textBoxAdress.Text, newPassword);
                    _users.Add(user);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Uzupełnij dane", "Podaj", MessageBoxButtons.OK);
            }
        }

        private void buttonAbort_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
