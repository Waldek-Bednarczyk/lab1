﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    //klasa uzytkownikow systemu
    [Serializable]
    public class User
    {
        public User() { } //niezbedne do serializacji pustych

        public User(string name, string surname, string adress, string password)
        {
            Adress = adress;
            Name = name;
            Surname = surname;
            Password = password;
        }

        public string Name { get; set;}

        public string Surname{ get; set;}

        public string Adress{ get; set;}

        public string Password { get; set;}
    }
}
