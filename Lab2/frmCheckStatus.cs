﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Lab2
{
    public partial class frmCheckStatus : Form
    {
        public string pathway = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\parcels.xml";
        public List<Parcel> parcels = new List<Parcel>();
        XmlSerializer serializer = new XmlSerializer(typeof(List<Parcel>));

        public frmCheckStatus()
        {
            InitializeComponent();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (File.Exists(pathway))
            {
                TextReader streamIN = new StreamReader(pathway);
                parcels = (List<Parcel>)serializer.Deserialize(streamIN);
                streamIN.Close();
            }
            foreach (var parcel in parcels)
            {
                if (textBoxNumber.Text != "" && parcel.Number == int.Parse(textBoxNumber.Text))
                {
                    labelStatus.Text = "Status paczki o podanym numerze to: \n" + parcel.Status;
                }
            }
            if(labelStatus.Text=="") MessageBox.Show("Podany numer paczki nie występuje w systemie","Sprawdź numer paczki", MessageBoxButtons.OK,MessageBoxIcon.Error);
        }
    }
}
