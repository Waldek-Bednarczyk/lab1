﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Lab2
{
    public partial class frmUserMenu : Form
    {
        public List<User> users = new List<User>();

        public string pathway = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\users.xml";
        XmlSerializer serializer = new XmlSerializer(typeof(List<User>));

        public frmUserMenu()
        {
            InitializeComponent();
            //wczytywanie do gridview z xml
            if(File.Exists(pathway))
            {
                TextReader streamIN = new StreamReader(pathway);
                users = (List<User>)serializer.Deserialize(streamIN);
                streamIN.Close();
                this.dataGridViewData.DataSource = users;
            }
            bindAndSave();
            comboBoxChoose.SelectedIndex = 0;
        }
        //jak w nazwie
        public void bindAndSave()
        {
            textBoxSearch.Text = "";
            BindingList<User> tmp = new BindingList<User>(users);
            dataGridViewData.DataSource = tmp;
            TextWriter streamOUT = new StreamWriter(pathway);
            serializer.Serialize(streamOUT, users);
            streamOUT.Close();
        }
        //uwidacznianie wszytskich
        public void setVisibleRows()
        {
            for (int j = 0; j < dataGridViewData.Rows.Count; j++)
            {
                dataGridViewData.Rows[j].Visible = true;
            }
        }
        //wyswietlenie kolejnego forma
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            frmNewUser frm2 = new frmNewUser(ref users);
            frm2.ShowDialog();
            bindAndSave();
        }
        //edycja użytkownika
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridViewData.SelectedRows)
            {
                int itemIndex = item.Index;
                string cellPassword = users[itemIndex].Password;
                string cellName = dataGridViewData.Rows[itemIndex].Cells[0].Value.ToString();
                string cellSurname = dataGridViewData.Rows[itemIndex].Cells[1].Value.ToString();
                string cellAdress = dataGridViewData.Rows[itemIndex].Cells[2].Value.ToString();
                frmNewUser frm2 = new frmNewUser(ref users, ref cellName, ref cellSurname, ref cellAdress, ref cellPassword, itemIndex);
                frm2.ShowDialog();
            }
            bindAndSave();
        }
        //usuwanie
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridViewData.SelectedRows)
            {
                dataGridViewData.Rows.RemoveAt(item.Index);
            }
            bindAndSave();
        }
        //wyszukiwanie
        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            string value = textBoxSearch.Text;
            int i = 0;
            setVisibleRows();
            if (value!= "")
            {
                if (comboBoxChoose.SelectedIndex == 0)
                {
                    foreach (var person in users)
                    {
                        if (!person.Name.Contains(value))
                        {
                            dataGridViewData.CurrentCell = null;
                            dataGridViewData.Rows[i].Visible = false;
                        }
                        i++;
                    }
                }
                if (comboBoxChoose.SelectedIndex == 1)
                {
                    foreach (var person in users)
                    {
                        if (!person.Surname.Contains(value))
                        {
                            dataGridViewData.CurrentCell = null;
                            dataGridViewData.Rows[i].Visible = false;
                        }
                        i++;
                    }
                }
                if (comboBoxChoose.SelectedIndex == 2)
                {
                    foreach (var person in users)
                    {
                        if (!person.Adress.Contains(value))
                        {
                            dataGridViewData.CurrentCell = null;
                            dataGridViewData.Rows[i].Visible = false;
                        }
                        i++;
                    }
                }
            }
        }

        private void comboBoxChoose_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxSearch.Text = "";
        }
    }
}
